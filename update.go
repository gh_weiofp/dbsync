package dbsync

import (
	"errors"
	"time"
)

// 插入数据时的配置信息
type UpdateOptions struct {
	Columns      []string               // 列名称
	TimeFields   []string               // time.Time时间类型的列名称列表
	FixedFields  map[string]interface{} // 固定的插入列
	UniqueFields []string               // 唯一键或主键的列名称列表
	SqlType      string                 // 数据库类型，目前支持MySQL和SQLite
}

func (m UpdateOptions) IsMySQL() bool {
	return m.SqlType == SqlTypeMySQL
}

func (m UpdateOptions) IsSqlite() bool {
	return m.SqlType == SqlTypeSqlite
}

// 通用插入数据
func update(
	db SQLCommon,
	tableName string,
	data [][]interface{},
	options UpdateOptions,
) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()
	// 按数据库类型处理
	if options.IsMySQL() {
		err = updateMySQL(db, tableName, data, options)
	} else if options.IsSqlite() {
		for _, dataItem := range data {
			if err = updateSqlite(db, tableName, dataItem, options); err != nil {
				return
			}
		}
	} else {
		err = errors.New("sql type is not supported")
	}
	return
}

// 类型转换方法
func convertTimeType(data interface{}) interface{} {
	if data != nil {
		return time.Unix(int64(data.(float64)), 0)
	} else {
		return nil
	}
}
